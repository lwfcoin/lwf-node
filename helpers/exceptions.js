'use strict';
/**
 * @namespace exceptions
 * @memberof module:helpers
 * @property {object} genesisPublicKey
 * @property {string} genesisPublicKey.mainnet
 * @property {string} genesisPublicKey.testnet
 * @property {Strin[]} senderPublicKey
 * @property {Strin[]} signatures
 * @property {Strin[]} multisignatures
 * @property {Strin[]} votes
 */	
module.exports = {
	blockRewards: [],
	genesisPublicKey: {
		mainnet: '477253710e8a0e1d6f436dbc4dfefbe65c8c8f7dab80eaa25742b0d552b4e4cb',
		testnet: 'efc4acad28300d0d7ebb59c232e12ec24309a2f885ad95414ca66a5fab8baf6b'
	},
	rounds: {
	},
	senderPublicKey: [
	],
	signatures: [
	],
	multisignatures: [
	],
	balance: [
	],
	votes: [
	]
};
